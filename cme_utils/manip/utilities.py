"""Provides some basic mathematical utility functions."""
import numpy as np
from random import random
import io
def rotation_matrix_from_to(a,b):
    """Returns rotation matrix R such that norm(b)*dot(R,a)/norm(a) = b.

    Args:
        a (numpy.ndarray): A 3-vector
        b (numpy.ndarray): Another 3-vector

    Returns:
        R (numpy.ndarray): The 3x3 rotation matrix that will would rotate a parallel to b.

    """
    a1 = a/np.linalg.norm(a)
    b1 = b/np.linalg.norm(b)
    theta = np.arccos(np.dot(a1,b1))
    if theta<1e-6 or np.isnan(theta):
        return np.identity(3)
    if np.pi-theta<1e-6: #TODO(Eric): verify correct
        d = np.array([1.,0,0])
        x = np.cross(a1,d)
    else:
        x = np.cross(a1,b1)
        x /= np.linalg.norm(x)
    A = np.array([ [0,-x[2],x[1]], [x[2],0,-x[0]], [-x[1],x[0],0] ])
    R = np.identity(3) + np.sin(theta)*A + (1.-np.cos(theta))*np.dot(A,A)
    return R

def shift_indicies_in_list(alist,min_i,shift):
    """Adds 'shift' to all indices>=min_i in a bond, angle, or dihedral list.

    Note that this function assumes that every element in alist is a list of strings
    where the first string is the type associated with the bond, angle, or dihedral.

    Args:
        alist (list): The list to be shifted.
        min_i (int): All indices at least this small will be shifted.
        shift (int): Amount to shift each shifted index.

    Returns:
        blist (list): shifted list
    """
    blist = []
    for i,a in enumerate(alist):
        b = [int(x) for x in a[1:]]
        for j,bb in enumerate(b):
            if bb>=min_i:
                b[j]=bb+shift
        blist.append([alist[i][0]] + [str(x)for x in b])
    return blist

def alphabetize(to_fix):
    """Cleans up bond, angle, and dihedral lists.

    Finds unique elements of bond, angle, and dihedral lists, with special care
    taken so that reversals an element type are considered (e.g., AAAB equals BAAA,
    but only the former is stored because it has lower lexicographical ordering)

    Args:
        to_fix (list): The first element of each element of to_fix is the type that will
            be checked and fixed.

    Returns:
        fixed_list
    """
    temp =list(set(to_fix)) #Get unique elements
    types = []
    for t in temp:
        if t==t[::-1]:
            types.append(t) #keep palindromes
        elif t<t[::-1]:
            types.append(t) #and keep the forward or reverse that has the lower lexicographical order
        else:
            types.append(t[::-1])
    types = sorted(list(set(types)))
    for i,t in enumerate(to_fix):
        if t[::-1] in types:
            to_fix[i]=t[::-1]
    return to_fix

def remove_items_not_containing(alist,key):
    """Removes all elements of a bond, angle, or dihedral list that don't contain 'key'.

    Note that this function assumes that every element in alist is a list of strings
    where the first string is the type associated with the bond, angle, or dihedral.

    Args:
        alist (list): The list to have elements removed from it
        key (str): Key to search for

    Returns:
        blist (list): shifted list
    """
    blist = []
    for a in alist:
        if key in a:
            blist.append(a)
    return blist

def remove_items_containing(alist,key):
    """Removes all elements of a bond, angle, or dihedral list containing key.

    Note that this function assumes that every element in alist is a list of strings
    where the first string is the type associated with the bond, angle, or dihedral.

    Args:
        alist (list): The list to have elements removed from it
        key (str): Key to search for

    Returns:
        blist (list): shifted list
    """
    blist = []
    for a in alist:
        if key not in a:
            blist.append(a)
    return blist

def loadMorphologyXML(xmlPath, sigma=1.0):
    # XML has SimDims as <box
    # Positions as <position and <image
    # Velocities as <velocity
    # Mass as <mass
    # Diameters as <diameter
    # Type as <type
    # "Body" as <body
    # Bonds as <bond, with each line as bondA, bondB, etc.
    # Angles as <angle, with each angle as angleA, angleB, etc.
    # Dihedral as <dihedral
    # Improper as <improper (usually none in xml)
    # Charge as <charge
    AtomDictionary = {'position': [], 'image': [], 'mass': [], 'diameter': [], 'type': [], 'body': [], 'bond': [], 'angle': [], 'dihedral': [], 'improper': [], 'charge': []}
    record = False
    with open(xmlPath, 'r') as xmlFile:
        xmlData = xmlFile.readlines()
        for line in xmlData:
            if ('</' in line):
                record = False
            elif ('<configuration' in line) or ('<box' in line):
                # Get configuration data from this line (timestep, natoms etc)
                splitLine = line.split(' ')
                for i in range(1, len(splitLine)):
                    equalsLoc = findIndex(splitLine[i], '=')
                    if equalsLoc is None:
                        # Skip any elements without equals
                        continue
                    quotationLoc = findIndex(splitLine[i], '"')
                    if ('.' in splitLine[i][quotationLoc[0] + 1:quotationLoc[1]]):
                        # Catch float in the value (excludes the = and quotation marks)
                        if ('<box' in line):
                            AtomDictionary[splitLine[i][:equalsLoc[0]]] = float(splitLine[i][quotationLoc[0] + 1:quotationLoc[1]])
                        else:
                            AtomDictionary[splitLine[i][:equalsLoc[0]]] = float(splitLine[i][quotationLoc[0] + 1:quotationLoc[1]])
                    else:
                        if ('<box' in line):
                            AtomDictionary[splitLine[i][:equalsLoc[0]]] = int(splitLine[i][quotationLoc[0] + 1:quotationLoc[1]])
                        else:
                            AtomDictionary[splitLine[i][:equalsLoc[0]]] = int(splitLine[i][quotationLoc[0] + 1:quotationLoc[1]])
            elif ('<position' in line):
                record = True
                recordType = 'position'
                continue
            elif ('<image' in line):
                record = True
                recordType = 'image'
                continue
            elif ('<mass' in line):
                record = True
                recordType = 'mass'
                continue
            elif ('<diameter' in line):
                record = True
                recordType = 'diameter'
                continue
            elif ('<type' in line):
                record = True
                recordType = 'type'
                continue
            elif ('<body' in line):
                record = True
                recordType = 'body'
                continue
            elif ('<bond' in line):
                record = True
                recordType = 'bond'
                continue
            elif ('<angle' in line):
                record = True
                recordType = 'angle'
                continue
            elif ('<dihedral' in line):
                record = True
                recordType = 'dihedral'
                continue
            elif ('<improper' in line):
                record = True
                recordType = 'improper'
                continue
            elif ('<charge' in line):
                record = True
                recordType = 'charge'
                continue
            # Now we know what the variable is, append it to the dictionary data
            if (record is True):
                if (recordType == 'position'):
                    # NOTE: VELOCITIES ARE NOT NORMALISED IN THE MORPHOLOGY FILE...DO THEY NEED TO BE SCALED BY SIGMA OR NOT? CURRENTLY THEY ARE.
                    # Write to dictionary as floats scaled by sigma
                    splitLine = line.split(' ')
                    # Remove the "\n"
                    splitLine[-1] = splitLine[-1][:-1]
                    if (len(splitLine) == 1):
                        AtomDictionary[recordType].append(float(splitLine[0]))
                        continue
                    for i in range(len(splitLine)):
                        splitLine[i] = float(splitLine[i])
                    AtomDictionary[recordType].append(splitLine)
                elif (recordType == 'mass') or (recordType == 'diameter') or (recordType == 'charge'):
                    # Write to dictionary as floats
                    splitLine = line.split(' ')
                    # Remove the "\n"
                    splitLine[-1] = splitLine[-1][:-1]
                    if (len(splitLine) == 1):
                        AtomDictionary[recordType].append(float(splitLine[0]))
                        continue
                    for i in range(len(splitLine)):
                        splitLine[i] = float(splitLine[i])
                    AtomDictionary[recordType].append(splitLine)
                elif (recordType == 'image') or (recordType == 'body'):
                    # Write to dictionary as int
                    splitLine = line.split(' ')
                    # Remove the "\n"
                    splitLine[-1] = splitLine[-1][:-1]
                    if (len(splitLine) == 1):
                        AtomDictionary[recordType].append(int(splitLine[0]))
                        continue
                    for i in range(len(splitLine)):
                        splitLine[i] = int(splitLine[i])
                    AtomDictionary[recordType].append(splitLine)
                elif (recordType == 'type'):
                    # Write to dictionary as str
                    splitLine = line.split(' ')
                    # Remove the "\n"
                    splitLine[-1] = splitLine[-1][:-1]
                    AtomDictionary[recordType].append(str(splitLine[0]))
                else:
                    #  (recordType == 'bond') or (recordType == 'angle') or (recordType == 'dihedral') or (recordType == 'improper')
                    # Write to dictionary as combination
                    splitLine = line.split(' ')
                    # Remove the "\n"
                    splitLine[-1] = splitLine[-1][:-1]
                    splitLine[0] = str(splitLine[0])
                    for i in range(1, len(splitLine)):
                        splitLine[i] = int(splitLine[i])
                    AtomDictionary[recordType].append(splitLine)
    if sigma != 1.0:
        AtomDictionary = scale(AtomDictionary, sigma)
    return AtomDictionary

def writeMorphologyXML(inputDictionary, outputFile, sigma = 1.0, check_inputs = True):
    # Firstly, scale everything by the inverse of the provided sigma value
    if sigma != 1.0:
        inputDictionary = scale(inputDictionary, 1.0 / sigma)
    # Now need to check the positions of the atoms to ensure that everything is correctly contained inside the box
    if check_inputs:
        tilt_factors = ["xy", "xz", "yz"]
        if any([inputDictionary[_] for _ in tilt_factors]):
            print("Can't check ⚛️tom images for cells with a tilt factor 😞")
        else:
            print("Checking wrapped positions before writing XML...")
            inputDictionary = checkWrappedPositions(inputDictionary)
    # print inputDictionary['image'][:20]
    # raw_input('HALT')
    # Add Boiler Plate first
    linesToWrite = ['<?xml version="1.0" encoding="UTF-8"?>\n', '<hoomd_xml version="1.4">\n', '<configuration time_step="0" dimensions="3" natoms="' + str(inputDictionary['natoms']) + '" >\n', '<box lx="' + str(inputDictionary['lx']) + '" ly="' + str(inputDictionary['ly']) + '" lz="' + str(inputDictionary['lz']) + '" xy="' + str(inputDictionary['xy']) + '" xz="' + str(inputDictionary['xz']) + '" yz="' + str(inputDictionary['yz']) + '" />\n']
    # Position
    linesToWrite.append('<position num="' + str(inputDictionary['natoms']) + '">\n')
    for positionData in inputDictionary['position']:
        linesToWrite.append(" ".join(str(coord) for coord in positionData) + '\n')
    linesToWrite.append('</position>\n')
    # Image
    linesToWrite.append('<image num="' + str(inputDictionary['natoms']) + '">\n')
    for imageData in inputDictionary['image']:
        linesToWrite.append(" ".join(str(coord) for coord in imageData) + '\n')
    linesToWrite.append('</image>\n')
    # Mass
    linesToWrite.append('<mass num="' + str(inputDictionary['natoms']) + '">\n')
    for massData in inputDictionary['mass']:
        linesToWrite.append(str(massData) + '\n')
    linesToWrite.append('</mass>\n')
    # Diameter
    linesToWrite.append('<diameter num="' + str(inputDictionary['natoms']) + '">\n')
    for diameterData in inputDictionary['diameter']:
        linesToWrite.append(str(diameterData) + '\n')
    linesToWrite.append('</diameter>\n')
    # Type
    linesToWrite.append('<type num="' + str(inputDictionary['natoms']) + '">\n')
    for typeData in inputDictionary['type']:
        linesToWrite.append(str(typeData) + '\n')
    linesToWrite.append('</type>\n')
    # Body
    linesToWrite.append('<body num="' + str(inputDictionary['natoms']) + '">\n')
    for bodyData in inputDictionary['body']:
        linesToWrite.append(str(bodyData) + '\n')
    linesToWrite.append('</body>\n')
    # Bond
    linesToWrite.append('<bond num="' + str(len(inputDictionary['bond'])) + '">\n')
    for bondData in inputDictionary['bond']:
        linesToWrite.append(" ".join(str(coord) for coord in bondData) + '\n')
    linesToWrite.append('</bond>\n')
    # Angle
    linesToWrite.append('<angle num="' + str(len(inputDictionary['angle'])) + '">\n')
    for angleData in inputDictionary['angle']:
        linesToWrite.append(" ".join(str(coord) for coord in angleData) + '\n')
    linesToWrite.append('</angle>\n')
    # Dihedral
    linesToWrite.append('<dihedral num="' + str(len(inputDictionary['dihedral'])) + '">\n')
    for dihedralData in inputDictionary['dihedral']:
        linesToWrite.append(" ".join(str(coord) for coord in dihedralData) + '\n')
    linesToWrite.append('</dihedral>\n')
    # Improper
    linesToWrite.append('<improper num="' + str(len(inputDictionary['improper'])) + '">\n')
    for improperData in inputDictionary['improper']:
        linesToWrite.append(" ".join(str(coord) for coord in improperData) + '\n')
    linesToWrite.append('</improper>\n')
    # Charge
    linesToWrite.append('<charge num="' + str(inputDictionary['natoms']) + '">\n')
    for chargeData in inputDictionary['charge']:
        linesToWrite.append(str(chargeData) + '\n')
    linesToWrite.append('</charge>\n')
    linesToWrite.append('</configuration>\n')
    linesToWrite.append('</hoomd_xml>\n')
    with open(outputFile, 'w+') as xmlFile:
        xmlFile.writelines(linesToWrite)
    print("XML file written to", str(outputFile) + "!")

def findIndex(string, character):
    '''This function returns the locations of an inputted character in an inputted string'''
    index = 0
    locations = []
    while index < len(string):
        if string[index] == character:
            locations.append(index)
        index += 1
    if len(locations) == 0:
        return None
    return locations

def scale(inputDictionary, scaleFactor):
    for ID, position in enumerate(inputDictionary['position']):
        # if ID == 24104:
        #     print "Initial Position =", inputDictionary['position'][ID], inputDictionary['image'][ID]
        inputDictionary['position'][ID] = list(scaleFactor * np.array(position))
        # if ID == 24104:
        #     print "Scaled Position =", inputDictionary['position'][ID], inputDictionary['image'][ID]
    for element in ['lx', 'ly', 'lz']:
        if element in inputDictionary:
            inputDictionary[element] *= scaleFactor
    return inputDictionary

def checkWrappedPositions(inputDictionary):
    atomPositions = np.array(inputDictionary['position'])
    atomImages = np.array(inputDictionary['image'])
    xhi = inputDictionary['lx'] / 2.0
    xlo = -inputDictionary['lx'] / 2.0
    yhi = inputDictionary['ly'] / 2.0
    ylo = -inputDictionary['ly'] / 2.0
    zhi = inputDictionary['lz'] / 2.0
    zlo = -inputDictionary['lz'] / 2.0
    for atomID in range(len(atomPositions)):
        while atomPositions[atomID][0] > xhi:
            atomPositions[atomID][0] -= inputDictionary['lx']
            atomImages[atomID][0] += 1
        while atomPositions[atomID][0] < xlo:
            atomPositions[atomID][0] += inputDictionary['lx']
            atomImages[atomID][0] -= 1
        while atomPositions[atomID][1] > yhi:
            atomPositions[atomID][1] -= inputDictionary['ly']
            atomImages[atomID][1] += 1
        while atomPositions[atomID][1] < ylo:
            atomPositions[atomID][1] += inputDictionary['ly']
            atomImages[atomID][1] -= 1
        while atomPositions[atomID][2] > zhi:
            atomPositions[atomID][2] -= inputDictionary['lz']
            atomImages[atomID][2] += 1
        while atomPositions[atomID][2] < zlo:
            atomPositions[atomID][2] += inputDictionary['lz']
            atomImages[atomID][2] -= 1
    inputDictionary['position'] = list(atomPositions)
    inputDictionary['image'] = list(atomImages)
    # print np.sum(np.absolute(atomPositions-tp) > 0.)
    return inputDictionary

def sphere_volume(radius=0.5):
    return 4.0 * np.pi * radius**3.0 / 3.0

def randInCube(L):
    x = L*random() - L/2.0
    y = L*random() - L/2.0
    z = L*random() - L/2.0
    return np.array([x, y, z])

def randVec():
    '''Returns a 3-vector uniformly distributed on a sphere'''
    rsq = 2.0
    while rsq >= 1.0:
        r1 = 1.0 - 2.0 * random()
        r2 = 1.0 - 2.0 * random()
        rsq = r1*r1 + r2*r2
    rh = (1.0-rsq)**.5
    return np.array([2.0*rh*r1, 2.0*rh*r2, 1.0-2.0*rsq])

def iround(x):  # rounds a float to the nearest integer
    y = round(x) - 0.5
    return int(y) + (y > 0)

def distance(a, b,L=50000.0):  # calculates the distance between a and b, with periodic cell L
    #TODO: Update for non-cubic boxes
    dx = b[0] - a[0]
    dx = dx - L*iround(dx/L)
    dy = b[1] - a[1]
    dy = dy - L*iround(dy/L)
    dz = b[2] - a[2]
    dz = dz - L*iround(dz/L)
    r = (dx**2 + dy**2 + dz**2)**.5
    return r

def overlap(a, b, L):
    for pa in a.particles:
        for pb in b.particles:
            if distance(pa.position, pb.position, L) < (pa.diameter + pb.diameter)/2.0:
                return True
    return False

def molOK(trial, molecules, L):
    for m in molecules:
        if distance(trial.position, m.position, L) < trial.Radius + m.Radius:
            if overlap(trial, m, L):
                return False
    return True

def randomizeMolecules(molecules, factor=2.5):
    v_total = 0.0
    for m in molecules:
        m.calcRadius()
        v_total = v_total+sphere_volume(m.Radius)
    v_total = v_total * factor
    L = v_total**(1.0/3.0)
    print("\n", L, "\n")
    setMolecules = []
    for m in molecules:
        print("setting", molecules.index(m), "/", len(molecules))
        trial = randInCube(L - 2.0*m.Radius)
        m.translate(trial, L)
        while not molOK(m, setMolecules, L):
            trial = randInCube(L-2.0*m.Radius)
            m.translate(trial, L)
        setMolecules.append(m)
    return L

def total_volume(molecules):
    vol = 0.
    for m in molecules:
        vol = vol + m.volume
    return vol

