from __future__ import print_function
from __future__ import division
from builtins import str
from builtins import range
from past.utils import old_div
import matplotlib.pyplot as plt
import numpy as np
import glob
import re
import matplotlib.cm as cm
import sys
import help_functions as hlp


def prep_data(data_file):



    a_data  = np.genfromtxt(data_file, delimiter=" ")
    temp = a_data[:,0]
    a_std = a_data[:,2]
    a_val = a_data[:,1]
    a_num_samples = a_data[:,3]
    a_std_err = a_std/np.sqrt(a_num_samples)

    return a_val, a_std_err, temp

def get_rel_error(a_rel_err, a_val, b_rel_err, b_val):

    return np.sqrt(((a_rel_err)**2)+((b_rel_err)**2))

if __name__ == "__main__":



    a_file = sys.argv[1]
    b_file = sys.argv[2]

    a_val, a_rel_err, a_temp = prep_data(a_file)
    b_val, b_rel_err, b_temp = prep_data(b_file)

    rel_error = get_rel_error(a_rel_err, a_val, b_rel_err, b_val)

    c = np.abs(a_val-b_val)
    c_error = rel_error
    c_tmp = a_temp


    with open("test-test-FIXEDenergy_diff_annealed.txt", "w") as text_file:
        text_file.write("#temp e_diff err\n")
        for _ in range(len(c)):
            text_file.write("{} {} {}\n".format(c_tmp[_],c[_],c_error[_]))
    #plt.errorbar(c_tmp,c,c_error)
    #plt.show()
