#!/opt/local/bin/python
#Calculates RDF from centers of mass of molecule selections
#rdf-com TOP TRAJ ATOMS/MOLECULE ID1 ID2 ID3... 
#Example: rdf-com restart.xml traj.dcd 20 4 8 11 #for perylene
#A single frame can be specified by adding frame # and one_frame = True arguments
#Example: rdf-come restart.hoomdxml traj.dcd 20 4 8 11 0 True #for perylene initial frame.
import matplotlib.pyplot as plt
import mdtraj as md
import numpy
import sys
import os
import re
import pickle

class RDF():
    def __init__(self):
        self.r = []
        self.rdf = []

    def calculate(self, Traj_file, Top_file, alst, frame = 0, one_frame = False):
        if one_frame == True:
            #NOTE: Calculating a single frame does not allow -1 to refer to the final frame.
            print("Loading frame {}.".format(frame))
            traj = md.load_frame(filename = Traj_file, index = frame, top=Top_file)
        else:
            print("Calculating for all frames.")
            traj = md.load(Traj_file, top = Top_file)

        atoms_per_molecule = alst[0]
        id1 = alst[1]
        id2 = alst[2]
        id3 = alst[3]

        p0 = numpy.arange(id1,traj.n_atoms,atoms_per_molecule)
        p1 = numpy.arange(id2,traj.n_atoms,atoms_per_molecule)
        p2 = numpy.arange(id3,traj.n_atoms,atoms_per_molecule)

        p01 = numpy.vstack((p0,p1)) #pairs of relid0's and 1's
        p02 = numpy.vstack((p0,p2)) 

        d01 = md.compute_displacements(traj,p01.T) #uses pbc's and fast distances
        d02 = md.compute_displacements(traj,p02.T)

        mols = traj.atom_slice(p0) #Do for all frames.
        coms = mols.xyz + (d01+d02)/3. #calc centers of mass from relid's

        traj2 = md.Trajectory(coms,mols.topology,unitcell_lengths=traj.unitcell_lengths,unitcell_angles=traj.unitcell_angles) #dummy trajectory 

        #Note max_r=minimum length/2 (otherwise minimum image convention not followed)
        r,rdf = md.compute_rdf(traj2,traj2.topology.select_pairs(selection1='all',selection2='all'),r_range=(0,numpy.min(traj2.unitcell_lengths)/2.))
        self.r, self.rdf = r, rdf
        return r, rdf

    def write_data(self, filename = 'rdf.txt', as_array = False):
        a = numpy.stack((numpy.array(self.r),numpy.array(self.rdf)), axis = 1)
        if as_array == True:
            a.dump(filename)
        else:
            numpy.savetxt(filename)

def plot_rdf(r, rdf, show = False, title=""):
    r = r[:100]
    rdf = rdf[:100]
    #plt.plot(10*r,rdf) #10x is nm->Angstrom unit conversion.
    plt.plot(numpy.arange(0,100),rdf) #10x is nm->Angstrom unit conversion.
    plt.xlabel('data point')
    #plt.xlabel('r ($\sigma$)')
    plt.xlim([0, 100])
    #plt.xlim([numpy.min(r)*10, numpy.amax(r)*10])
    plt.ylabel('data')
    plt.title(title)
    #plt.ylabel('g(r)')
    plt.savefig("rdf.pdf")
    if show == True:
        plt.show()

if __name__ == "__main__":
    rdf = RDF()
    r, rdf = rdf.calculate(sys.argv[2], sys.argv[1], [int(i) for i in sys.argv[3:7]], frame = 50, one_frame = True )
    phi, T = sys.argv[7], sys.argv[8]
    plot_rdf(r, rdf, show=False, title=r"$\phi$={} T={}".format(phi, T))
