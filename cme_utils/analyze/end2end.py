import numpy as np
import mdtraj as md
import cme_utils
import sys

if __name__ == "__main__":

    TOP_FILE = sys.argv[1]
    START = 6 #int(sys.argv[2])
    END = 211 #int(sys.argv[3])

    xml = cme_utils.manip.hoomd_xml.hoomd_xml()
    xml.read(TOP_FILE)

    xyz = xml.config.find('position').text.split()
    image = xml.config.find('image').text.split()
    lx = xml.config.find('box').attrib['lx']
    lx = float(lx)
    N = int(len(xyz)/3)

    xyz_list = np.zeros(shape=(N,3))
    image_list = np.zeros(shape=(N,3))

    for i in range(N):
        xyz_list[i]=np.array([float(x) for x in xyz[i*3:i*3+3]])
        image_list[i]=np.array([int(x) for x in image[i*3:i*3+3]])
        xyz_list[i]=image_list[i]*lx+xyz_list[i]




    n_chains = 197 # This is a dirty magic number, should be an input

    atoms_per_chain = N/n_chains


    e2e_dist = np.zeros(n_chains)
    for i, chain_num in enumerate(range(n_chains)):
        e2e_dist[i] = np.linalg.norm(xyz_list[ atoms_per_chain * chain_num+START] - xyz_list[atoms_per_chain * chain_num+END])

    mean_dist = np.mean(e2e_dist)
    std_dist = np.std(e2e_dist)
    print(mean_dist, std_dist)

    #print(np.linalg.norm((xyz_list[27846]- xyz_list[28051])))
    #print(xyz_list[27846],xyz_list[28051])
    '''
    bb_index = [x for x in range(idx_start,n_atoms,idx_stride)]
    bb_xyz_raw = [t.xyz[-1, i, :] for i in bb_index]


    distance_list = []
    distance_list = np.array(distance_list)
    for i in range(n_chains):
        #print(i)
        for j in range(num_mon):
            x1 = bb_xyz_raw[i * num_mon + j]
            x1_idx = bb_index[i * num_mon + j]
            x2_list_front = bb_xyz_raw[:i*num_mon]
            x2_list_front_idx = bb_index[:i*num_mon]
            
            x2_list_back = bb_xyz_raw[i*num_mon+num_mon:]
            x2_list_back_idx = bb_index[i*num_mon+num_mon:]


            x2_list = x2_list_front + x2_list_back
            x2_list_idx =  x2_list_front_idx + x2_list_back_idx
            
            distance_list = np.fromiter([np.linalg.norm(x1 - x2)*10 for x2 in x2_list], dtype=float, count=n_chains)
            for m,x2 in enumerate(x2_list_idx):
                if x1_idx == x2:
                    print("id match ", x1_idx, x2)
        mindist = min(distance_list)
        print(i,mindist,np.where(mindist == distance_list)[0][0])

    '''

    #print(np.linalg.norm(bb_xyz_raw[1]-bb_xyz_raw[2])*10)
    #print(md.compute_distances(t[-1],[[96,48]])*10)
